<?php
// single file implementation - easier to copy?
function define_if_not_exists($stuff,$value) {
	defined($stuff) or define($stuff,$value);
}
define('MY1CFGTHIS',dirname(__FILE__).'/include/'.basename(__FILE__).".conf");
if (file_exists(MY1CFGTHIS)) include_once MY1CFGTHIS;
define_if_not_exists('APP_MY1KEY','xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
define_if_not_exists('API_SERVER','http://localhost');
define_if_not_exists('API_SECURE',false);
class ServerAPI {
	private $_apikey;
	private $_apiurl;
	private $_algo; // maybe use aes-256-gcm?
	private $_hash;
	private $_size; // iv length
	public function __construct($apikey,$apisrv,
			$algo='aes-256-ctr',$hash='sha256') {
		$this->_apikey = $apikey;
		$this->_apiurl = $apisrv;
		$this->_algo = $algo;
		$this->_hash = $hash;
		$this->_size = null;
	}
	public function request($method,$target,$params,$secure=false) {
		$headers = array();
		$result = [];
		// my1apisrv always return json!
		$headers[] = 'Accept: application/json';
		$paramz = json_encode($params);
		if ($secure) {
			// we are sending base64-encoded-encrypted json - treat as text
			$headers[] = 'Content-Type: application/text';
			// check if we can go secure
			if ($this->_size===null) {
				if (!in_array($this->_algo,openssl_get_cipher_methods(true)))
					$this->throw_this("Invalid algo '$algo'!");
				if (!in_array($this->_hash,openssl_get_md_methods(true)))
					$this->throw_this("Invalid hash '$hash'!");
				$this->_size = openssl_cipher_iv_length($this->_algo);
			}
			$paramz = $this->encrypt($paramz,$this->_apikey);
		}
		$apicall = curl_init();
		$curl_target = $this->_apiurl."".$target;
		curl_setopt($apicall,CURLOPT_URL,$curl_target);
		curl_setopt($apicall,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($apicall,CURLOPT_CUSTOMREQUEST,$method);
		curl_setopt($apicall,CURLOPT_POSTFIELDS,$paramz);
		curl_setopt($apicall,CURLOPT_HTTPHEADER,$headers);
		$output = curl_exec($apicall);
		$result = json_decode($output,true);
		if ($secure) {
			// cryptor is guaranteed here
			if (!isset($result['secure'])||$result['secure']!==true) {
				$result['api_emsg'] = "INSECURE";
				$result['api_flag'] = false;
			} else if (!isset($result['data'])) {
				$result['api_emsg'] = "NO DATA!";
				$result['api_flag'] = false;
			} else {
				$normal = $this->decrypt($params,$this->_apikey);
				$result['data'] = json_encode($normal);
				$result['api_emsg'] = "DECRYPTED";
				$result['api_flag'] = true;
			}
		}
		return $result;
	}
	protected function throw_this($error) {
		throw new Exception("[".get_class($this)."] ** ".$error);
	}
	protected function encrypt($text,$key) {
		$iv = openssl_random_pseudo_bytes($this->_size,$test);
		if (!$test)
			$this->throw_this("Key is not strong enough!");
		$hash = openssl_digest($key,$this->_hash,true);
		$opts = OPENSSL_RAW_DATA;
		$ores = openssl_encrypt($text,$this->_algo,$hash,$opts,$iv);
		if ($ores===false)
			$this->throw_this("Encryption error! (".openssl_error_string().")");
		$temp = $iv . $ores;
		return base64_encode($temp);
	}
	protected function decrypt($text,$key) {
		$next = base64_decode($text);
		// and do an integrity check on the size.
		if (strlen($next) < $this->_size)
			$this->throw_this("Length error!");
		$iv = substr($next,0,$this->_size);
		$text = substr($next,$this->_size);
		$hash = openssl_digest($key,$this->_hash,true);
		$opts = OPENSSL_RAW_DATA;
		$ires = openssl_decrypt($text,$this->_algo,$hash,$opts,$iv);
		if ($ires===false)
			$this->throw_this("Encryption error! (".openssl_error_string().")");
		return $ires;
	}
}
try {
	$apikey = APP_MY1KEY;
	$apiurl = API_SERVER;
	$secure = API_SECURE;
	$method = "GET";
	$target = "/api";
	$option = "";
	// get raw http content - not encrypted! maybe use https?
	$paramz = file_get_contents("php://input");
	$params = json_decode($paramz,true);
	$debug_ = "";
	foreach ($_GET as $key => $val) {
		switch ($key) {
		case 'target': $target = htmlspecialchars($val); break;
		case 'option': $option = htmlspecialchars($val); break;
		case 'debug_': $debug_ = htmlspecialchars($val); break;
		default: $params[$key] = htmlspecialchars($val); break;
		}
	}
	if ($option!="") {
		$option = explode(',',$option);
		$option = implode('/',$option);
		$target = $target.'/'.$option;
	}
	$server = new ServerAPI($apikey,$apiurl);
	$result = $server->request($method,$target,$params,$secure);
	if (!isset($result['flag'])) {
		throw new Exception("API Error! [".json_encode($result)."]");
	}
} catch (Exception $error) {
	$result['emsg'] = $error->getMessage();
	$result['flag'] = false;
}
if (strtoupper($debug_)==="YES") {
	$result['apiurl'] = $apiurl;
	$result['apitgt'] = $target;
	$result['secure'] = $secure;
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($result).PHP_EOL;
exit();
?>
