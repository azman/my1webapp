//------------------------------------------------------------------------------
class MY1Date {
	constructor(dateString,isUTC) {
		this.date = MY1Date.DateString(dateString,isUTC);
	}
	static DateString(dateString,isUTC) {
		if (isUTC===undefined||isUTC!==true) {
			return MY1Date.DateStringLOC(dateString);
		} else {
			return MY1Date.DateStringUTC(dateString);
		}
	}
	static DateStringUTC(dateString) {
		// create Date object in UTC
		let temp = new Date();
		if (dateString===undefined)
			return temp;
		if (dateString.length==8||dateString.length==14) {
			let tempString = dateString.replace(/[^0-9]/g, "");
			if (tempString.length!=dateString.length)
				return null;
			while (dateString.length<14) dateString += '0';
			temp.setUTCFullYear(parseInt(dateString.substr(0,4),10),
				parseInt(dateString.substr(4,2),10)-1,
				parseInt(dateString.substr(6,2),10));
			temp.setUTCHours(parseInt(dateString.substr(8,2),10));
			temp.setUTCMinutes(parseInt(dateString.substr(10,2),10));
			temp.setUTCSeconds(parseInt(dateString.substr(12,2),10));
		} else if (dateString.length==10||dateString.length==19) {
			let tempString = dateString.replace(/[^0-9\-T:]/g, "");
			if (tempString.length!=dateString.length)
				return null;
			if (dateString.length==10) dateString += 'T00:00:00';
			temp.setUTCFullYear(parseInt(dateString.substr(0,4),10),
				parseInt(dateString.substr(5,2),10)-1,
				parseInt(dateString.substr(8,2),10));
			temp.setUTCHours(parseInt(dateString.substr(11,2),10));
			temp.setUTCMinutes(parseInt(dateString.substr(14,2),10));
			temp.setUTCSeconds(parseInt(dateString.substr(17,2),10));
		}
		return temp;
	}
	static DateStringLOC(dateString) {
		// create Date object in localtime
		let temp = new Date();
		if (dateString===undefined)
			return temp;
		if (dateString.length==8||dateString.length==14) {
			let tempString = dateString.replace(/[^0-9]/g, "");
			if (tempString.length!=dateString.length)
				return null;
			while (dateString.length<14) dateString += '0';
			temp.setFullYear(parseInt(dateString.substr(0,4),10),
				parseInt(dateString.substr(4,2),10)-1,
				parseInt(dateString.substr(6,2),10));
			temp.setHours(parseInt(dateString.substr(8,2),10));
			temp.setMinutes(parseInt(dateString.substr(10,2),10));
			temp.setSeconds(parseInt(dateString.substr(12,2),10));
		} else if (dateString.length==10||dateString.length==19) {
			let tempString = dateString.replace(/[^0-9\-T:]/g, "");
			if (tempString.length!=dateString.length)
				return null;
			if (dateString.length==10) dateString += 'T00:00:00';
			temp.setFullYear(parseInt(dateString.substr(0,4),10),
				parseInt(dateString.substr(5,2),10)-1,
				parseInt(dateString.substr(8,2),10));
			temp.setHours(parseInt(dateString.substr(11,2),10));
			temp.setMinutes(parseInt(dateString.substr(14,2),10));
			temp.setSeconds(parseInt(dateString.substr(17,2),10));
		}
		return temp;
	}
	ISOStringUTC() {
		// print iso-8601 formatted utc datestring
		return this.date.toISOString().substr(0,19);
	}
	MY1StringUTC() {
		// print MY formatted utc datestring
		let dateString = this.date.getUTCFullYear().toString();
		let tempString = (this.date.getUTCMonth()+1).toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		tempString = this.date.getUTCDate().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		tempString = this.date.getUTCHours().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		tempString = this.date.getUTCMinutes().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		tempString = this.date.getUTCSeconds().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		return dateString;
	}
	ISOString() {
		// print iso-8601 formatted localtime datestring
		let dateString = this.date.getFullYear().toString();
		while (dateString.length<4) dateString = "0"+dateString;
		dateString += "-";
		let tempString = (this.date.getMonth()+1).toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		dateString += "-";
		tempString = this.date.getDate().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		dateString += "T";
		tempString = this.date.getHours().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		dateString += ":";
		tempString = this.date.getMinutes().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		dateString += ":";
		tempString = this.date.getSeconds().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		return dateString;
	}
	MY1String() {
		// print MY formatted localtime datestring
		let dateString = this.date.getFullYear().toString();
		let tempString = (this.date.getMonth()+1).toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		tempString = this.date.getDate().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		tempString = this.date.getHours().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		tempString = this.date.getMinutes().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		tempString = this.date.getSeconds().toString();
		dateString += (tempString[1]?tempString:"0"+tempString);
		return dateString;
	}
	DateText() {
		// print human readable date (always in local time)
		let tempString = this.date.getDate().toString();
		let dateString = (tempString[1]?tempString:"0"+tempString[0]);
		dateString += "/";
		tempString = (this.date.getMonth()+1).toString();
		dateString += (tempString[1]?tempString:"0"+tempString[0]);
		dateString += "/";
		dateString += this.date.getFullYear().toString();
		return dateString;
	}
	TimeText() {
		// print human readable time (always in local time)
		let tempString = this.date.getHours().toString();
		let dateString = (tempString[1]?tempString:"0"+tempString[0]);
		dateString += ":";
		tempString = this.date.getMinutes().toString();
		dateString += (tempString[1]?tempString:"0"+tempString[0]);
		dateString += ":";
		tempString = this.date.getSeconds().toString();
		dateString += (tempString[1]?tempString:"0"+tempString[0]);
		return dateString;
	}
	FullText() {
		// print human readable time & date (always in local time)
		return this.TimeText()+' '+this.DateText();
	}
	static IsLeapYear(year) {
		return ((year%4==0&&year%100!=0)||(year%400==0))?true:false;
	}
	static MonthDays(month,leap=false) {
		const MaxDays = [31,28,31,30,31,30,31,31,30,31,30,31];
		// get total days in month  - static function
		let maxday = MaxDays[month];
		if (leap&&month==1) maxday++;
		return maxday;
	}
	TotalMonthDays() {
		// get total days this month
		let month = this.date.getMonth();
		let year = this.date.getFullYear();
		let leap = MY1Date.IsLeapYear(year);
		return MY1Date.MonthDays(month,leap);
	}
	WeekDay() {
		return this.date.getDay(); // 0-6
	}
	NextDay() {
		let day = this.date.getDate();
		let month = this.date.getMonth();
		let year = this.date.getFullYear();
		let leap = MY1Date.IsLeapYear(year);
		let temp = MY1Date.MonthDays(month,leap);
		if (++day>temp) {
			day = 1;
			month++;
			if (month>11)
			{
				month = 0;
				year++;
			}
		}
		this.date.setFullYear(year,month,day);
	}
	PrevDay() {
		let day = this.date.getDate();
		let month = this.date.getMonth();
		let year = this.date.getFullYear();
		if (--day<1) {
			month--;
			if (month<0)
			{
				month = 11;
				year--;
			}
			let leap = MY1Date.IsLeapYear(year);
			let temp = MY1Date.MonthDays(month,leap);
			day = temp;
		}
		this.date.setFullYear(year,month,day);
	}
	AddDays(days) {
		// add days
		let day = this.date.getDate();
		let month = this.date.getMonth();
		let year = this.date.getFullYear();
		let leap = MY1Date.IsLeapYear(year);
		let remains = MY1Date.MonthDays(month,leap)-day;
		while (days>remains)
		{
			days -= remains;
			month++;
			if (month>11)
			{
				month = 0;
				year++;
				leap = MY1Date.IsLeapYear(year);
			}
			remains = MY1Date.MonthDays(month,leap);
			day = 0;
		}
		day += days;
		this.date.setFullYear(year,month,day);
	}
	SubDays(days) {
		// subtract days
		let day = this.date.getDate();
		let month = this.date.getMonth();
		let year = this.date.getFullYear();
		let leap = MY1Date.IsLeapYear(year);
		while (days>day)
		{
			days -= day;
			month--;
			if (month<0)
			{
				month = 11
				year--;
				leap = MY1Date.IsLeapYear(year);
			}
			day = MY1Date.MonthDays(month,leap);
		}
		day -= days;
		this.date.setFullYear(year,month,day);
	}
	NextMonth() {
		let day = this.date.getDate();
		let month = this.date.getMonth();
		let year = this.date.getFullYear();
		if (++month>11) {
			month = 0;
			year++;
		}
		let leap = MY1Date.IsLeapYear(year);
		let temp = MY1Date.MonthDays(month,leap);
		if (day>temp) day = temp;
		this.date.setFullYear(year,month,day);
	}
	PrevMonth() {
		let day = this.date.getDate();
		let month = this.date.getMonth();
		let year = this.date.getFullYear();
		if (--month<0) {
			month = 11;
			year--;
		}
		let leap = MY1Date.IsLeapYear(year);
		let temp = MY1Date.MonthDays(month,leap);
		if (day>temp) day = temp;
		this.date.setFullYear(year,month,day);
	}
}
//------------------------------------------------------------------------------
