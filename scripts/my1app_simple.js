//------------------------------------------------------------------------------
const API_TARGET = "/api";
//------------------------------------------------------------------------------
class MY1Desk extends MY1DeskMain {
	constructor(title,doapp) {
		super(title);
		this.main = doapp;
		this.build();
	}
	build() {
		this.makeHead(this.title);
		this.makeContent();
	}
	makeContent() {
		// create div for main content
		this.view = this.append_element('div');
		this.view.classList.add("w3-main");
		this.view.style = "margin-top:43px;";
	}
	// cookies!
	saveUser() {
		this.createCookie("uuid",this.user.uuid,1);
		this.createCookie("name",this.user.user,1);
		this.createCookie("nick",this.user.nick,1);
	}
	loadUser() {
		let uuid = this.readCookie("uuid");
		let name = this.readCookie("name");
		let nick = this.readCookie("nick");
		if (uuid!==null&&name!==null&&nick!==null) {
			this.updateUser(uuid,name,nick);
			return true;
		}
		return false;
	}
	doneUser() {
		this.removeCookie("uuid");
		this.removeCookie("name");
		this.removeCookie("nick");
	}
}
//------------------------------------------------------------------------------
class MY1App extends MY1Logger {
	constructor(title,uinfo=null) {
		super();
		this.desk = new MY1Desk(title,this);
		this.date = new MY1Date();
		this.create_basic_page();
	}
	create_basic_page() {
		this.desk.wipeContent();
		// build contents - need these to be more customizable
		this.desk.makeContent_header("Information Summary","fa-clipboard-list");
		// block items
		let cont = new MY1ViewItemBlock(this.desk);
		let ucnt = cont.makeItem("w3-red","fa-tower-cell","??","Random (5s)");
		ucnt.apid = new MY1DataAPI(API_TARGET+"/things/random",ucnt.elem);
		ucnt.apid.show_pick = 'random';
		ucnt.apid.moreAPI = 5000;
		ucnt.apid.get_data();
		ucnt.icon.classList.add("fa-beat");
		let tcnt = cont.makeItem("w3-blue","fa-share-alt","??","Things");
		tcnt.apid = new MY1DataAPI(API_TARGET+"/things",tcnt.elem);
		tcnt.apid.get_data();
		cont.fit();
		// fill it up with samples
		cont = new MY1ViewItemBlock(this.desk);
		cont.makeItem("w3-red","fa-comment","23","Info 1");
		cont.makeItem("w3-blue","fa-eye","49","Info 2");
		cont.makeItem("w3-teal","fa-share-alt","32","Info 3");
		cont.makeItem("w3-orange","fa-users","33","Info 4");
		cont.fit();
		// panel items
		cont = new MY1ViewItemPanel(this.desk,false,
			"images/region.jpg","Panel Image","Panel List");
		cont.makeItem("fa-user","w3-text-blue","Info 1","Data 1");
		cont.makeItem("fa-bell","w3-text-red","Info 2","Data 2");
		cont.makeItem("fa-users","w3-text-yellow","Info 3","Data 3");
		cont.makeItem("fa-comment","w3-text-red","Info 4","Data 4");
		cont.makeItem("fa-bookmark","w3-text-blue","Info 5","Data 5");
		cont.makeItem("fa-laptop","w3-text-red","Info 6","Data 6");
		// chart items
		cont = new MY1ViewItemChart(this.desk,"Statistics: Bar Charts");
		cont.makeItem("Stat 1","w3-green","w3-grey",25);
		cont.makeItem("Stat 2","w3-orange","w3-grey",50);
		cont.makeItem("Stat 3","w3-red","w3-grey",75);
		cont.hide();
		setTimeout(function(v) { v.show(); },5000,cont);
		// table data
		cont = new MY1ViewItemTable(this.desk,"Statistics: Table Data");
		cont.makeItem("Data 1","101");
		cont.makeItem("Data 2","102");
		cont.makeItem("Data 3","103");
		cont.makeItem("Data 4","104");
		cont.moreButton("More Data  ","w3-dark-grey");
		// links
		cont = new MY1ViewItemLinks(this.desk);
		cont.makeLink("Group 1","w3-border-green");
		cont.makeLinkItem(0,"Link 1");
		cont.makeLinkItem(0,"Link 2");
		cont.makeLinkItem(0,"Link 3");
		cont.makeLink("Group 2","w3-border-red");
		cont.makeLinkItem(1,"Link 1");
		cont.makeLinkItem(1,"Link 2");
		cont.makeLinkItem(1,"Link 3");
		cont.makeLink("Group 3","w3-border-orange");
		cont.makeLinkItem(2,"Link 1");
		cont.makeLinkItem(2,"Link 2");
		// footer
		this.desk.makeContent_footer();
	}
}
//------------------------------------------------------------------------------
