//------------------------------------------------------------------------------
class MY1Base {
	class_name() {
		return this.constructor.name;
	}
	// for multiple inheritance!
	assimilate(...list) {
		for (let that of list) {
			let curr = that.prototype;
			let last = curr;
			let pick = [];
			do { // make sure we start with user defined base class!
				if (curr.constructor.name==="Object")
					break;
				pick.push(curr);
			} while ((curr=Object.getPrototypeOf(curr)));
			while ((curr=pick.pop())) {
				//console.log("## curr:"+curr.constructor.name);
				//console.log(curr);
				for (let key of Object.getOwnPropertyNames(curr)) {
					if (key!=='constructor') {
						if (!this[key]) // do not override existing!
							this[key] = curr[key];
					}
				}
			}
			// default constructors only!
			let temp = new last['constructor']();
			Object.assign(this,temp);
		}
	}
}
//------------------------------------------------------------------------------
// requires: my1date.js
//------------------------------------------------------------------------------
class MY1Logger {
	constructor() {
		this.dbug = false;
		this.cons = false;
	}
	show_debug(mesg) {
		if (this.dbug) console.log('@@ '+mesg);
	}
	show_error(mesg) {
		console.log('** '+mesg);
	}
	show_info(mesg) {
		console.log("## "+mesg);
	}
	show_console(mesg) {
		if (this.cons) // cannot remember WHY i do this...
			queueMicrotask(console.log.bind(console,mesg));
	}
	log_debug(mesg) {
		let tdate = new MY1Date();
		this.show_debug("["+tdate.FullText()+"] "+mesg);
	}
	log_error(mesg) {
		let tdate = new MY1Date();
		this.show_error("["+tdate.FullText()+"] "+mesg);
	}
	log_info(mesg) {
		let tdate = new MY1Date();
		this.show_info("["+tdate.FullText()+"] "+mesg);
	}
}
//------------------------------------------------------------------------------
