//------------------------------------------------------------------------------
// demo for my1apisrv
const API_TARGET = "/api";
//------------------------------------------------------------------------------
class MY1DeskDemo extends MY1DeskMain {
	constructor(title,doapp) {
		super(title);
		this.main = doapp;
		this.build();
		// build menu
		this.makeMenuItem("Users","fa-users");
		this.makeMenuItem("Things","fa-share-alt");
	}
	// override stuff to customize
	makeUser() {
		this.user = new MY1UserData(this);
		this.user.assimilate(MY1FetchAPI);
		//this.user.dbug = true;
	}
	onLogin() {
		this.doAlert("Welcome back, "+this.user.nick+"!","yay");
		this.main.create_intro_page();
	}
	onLogout() {
		this.doAlert("Logged out","yay");
		this.main.create_basic_page();
	}
}
//------------------------------------------------------------------------------
class MY1App extends MY1Logger {
	constructor(title,uinfo=null) {
		super();
		this.desk = new MY1DeskDemo(title,this);
		this.date = new MY1Date();
		this.create_basic_page();
		if (uinfo!==null && 'uuid' in uinfo &&
				'user' in uinfo && 'nick' in uinfo) {
			this.desk.updateUser(uinfo.uuid,uinfo.user,uinfo.nick);
		}
	}
	doLogin(data) {
		this.desk.user.doLogin(data);
	}
	doLogout() {
		this.desk.user.doLogout();
	}
	create_basic_page() {
		this.desk.wipeContent();
		// build contents - need these to be more customizable
		this.desk.makeContent_header("Information Summary","fa-clipboard-list");
		// block items
		let cont = new MY1ViewItemBlock(this.desk);
		let ucnt = cont.makeItem("w3-red","fa-spinner","??","Random (5s)");
		ucnt.apid = new MY1DataAPI(API_TARGET+"/things/random",ucnt.elem);
		ucnt.apid.show_pick = 'random';
		ucnt.apid.moreAPI = 5000;
		ucnt.apid.get_data();
		let tcnt = cont.makeItem("w3-blue","fa-share-alt","??","Things");
		tcnt.apid = new MY1DataAPI(API_TARGET+"/things",tcnt.elem);
		tcnt.apid.get_data();
		cont.fit();
		// footer
		this.desk.makeContent_footer();
	}
	create_intro_page() {
		this.desk.wipeContent();
		this.desk.makeContent_header("Intro","fa-cloud");
		let cont = new MY1ViewItemBlock(this.desk);
		let rcnt = cont.makeItem("w3-blue","fa-spinner","??","Random (5s)");
		rcnt.apid = new MY1DataAPI(API_TARGET+"/things/random",rcnt.elem);
		rcnt.apid.show_pick = 'random';
		rcnt.apid.moreAPI = 5000;
		rcnt.apid.get_data();
		cont.fit();
		this.desk.makeContent_header("Data","fa-clipboard-list");
		this.create_demo_panel();
		this.create_demo_chart();
		this.create_demo_table();
		this.create_demo_links();
		// footer
		this.desk.makeContent_footer();
	}
	create_demo_panel() {
		// panel items
		let cont = new MY1ViewItemPanel(this.desk,false,
			"images/region.jpg","Panel Image","Panel List");
		cont.makeItem("fa-user","w3-text-blue","Info 1","Data 1");
		cont.makeItem("fa-bell","w3-text-red","Info 2","Data 2");
		cont.makeItem("fa-users","w3-text-yellow","Info 3","Data 3");
		cont.makeItem("fa-comment","w3-text-red","Info 4","Data 4");
		cont.makeItem("fa-bookmark","w3-text-blue","Info 5","Data 5");
		cont.makeItem("fa-laptop","w3-text-red","Info 6","Data 6");
	}
	create_demo_chart() {
		// chart items
		let cont = new MY1ViewItemChart(this.desk,"Statistics: Bar Charts");
		cont.makeItem("Stat 1","w3-green","w3-grey",25);
		cont.makeItem("Stat 2","w3-orange","w3-grey",50);
		cont.makeItem("Stat 3","w3-red","w3-grey",75);
		cont.hide();
		setTimeout(()=>cont.show(),5000);
	}
	create_demo_table() {
		let cont = new MY1ViewItemTable(this.desk,"Statistics: Table Data");
		cont.makeItem("Data 1","101");
		cont.makeItem("Data 2","102");
		cont.makeItem("Data 3","103");
		cont.makeItem("Data 4","104");
		cont.moreButton("More Data  ","w3-dark-grey");
	}
	create_demo_links() {
		let cont = new MY1ViewItemLinks(this.desk);
		cont.makeLink("Group 1","w3-border-green");
		cont.makeLinkItem(0,"Link 1");
		cont.makeLinkItem(0,"Link 2");
		cont.makeLinkItem(0,"Link 3");
		cont.makeLink("Group 2","w3-border-red");
		cont.makeLinkItem(1,"Link 1");
		cont.makeLinkItem(1,"Link 2");
		cont.makeLinkItem(1,"Link 3");
		cont.makeLink("Group 3","w3-border-orange");
		cont.makeLinkItem(2,"Link 1");
		cont.makeLinkItem(2,"Link 2");
	}
}
//------------------------------------------------------------------------------
