//------------------------------------------------------------------------------
// requires: my1base.js , my1call.js and sha512.js
//------------------------------------------------------------------------------
class MY1ViewBase {
	constructor(elem=null) {
		this.elem = document.body;
		if (elem!==null && elem instanceof Element)
			this.elem = elem;
	}
	build() {
		let buff = this.append_element('div');
		buff.innerHTML = "Hello World!";
	}
	append_element(eltype,thatid) {
		let ndiv = document.createElement(eltype);
		if (thatid!==undefined)
			ndiv.id = thatid;
		this.elem.appendChild(ndiv);
		return ndiv;
	}
	insert_element(eltype,thatid) {
		let ndiv = document.createElement(eltype);
		if (thatid!==undefined)
			ndiv.id = thatid;
		this.elem.insertBefore(ndiv,parent.firstChild);
		return ndiv;
	}
}
//------------------------------------------------------------------------------
class MY1DeskBase extends MY1ViewBase {
	constructor() {
		super();
		this.cfix = "my1_";
	}
	createCookie(name,value,days) {
		// create cookie - source: http://www.quirksmode.org/js/cookies.html
		name = this.cfix+name;
		let expires = "";
		if (days) {
			let date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
		}
		document.cookie = name+"="+value+expires+"; path=/";
	}
	readCookie(name) {
		// read cookie
		name = this.cfix+name;
		let nameEQ = name + "=";
		let ca = document.cookie.split(';');
		for(let i=0;i < ca.length;i++) {
			let c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ)==0)
				return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
	removeCookie(name) {
		// remove cookie
		name = this.cfix+name;
		createCookie(name,"",-1);
	}
	// maintain this until i find better solution!
	create_element(eltype,parent,doprev,thatid) {
		// create child element
		let ndiv = document.createElement(eltype);
		if (thatid!==undefined)
			ndiv.id = thatid;
		if (parent===undefined)
			parent = this.elem;
		if (doprev===undefined||doprev!==true)
			parent.appendChild(ndiv);
		else
			parent.insertBefore(ndiv,parent.firstChild);
		return ndiv;
	}
}
//------------------------------------------------------------------------------
class MY1DeskMain extends MY1DeskBase {
	constructor(title) {
		super();
		this.title = title;
		this.strLogout = "doLogout();";
		this.alertTime = 3000; // ms
	}
	build() {
		this.makeUser();
		this.makeHead(this.title);
		this.makeMenu();
		this.makeContent();
	}
	makeUser() {
		this.user = new MY1UserData(this);
	}
	makeHead(title) {
		this.head = new MY1DeskHead(this,title);
		let that = this;
		let fun1 = function(){that.doMenu(true);}
		this.head.pick.addEventListener('click',fun1);
	}
	makeMenu() {
		this.menu = new MY1MenuOver(this);
		this.menu.update();
		let that = this;
		let fun1 = function(){that.doMenu(true);}
		this.menu.olay.addEventListener('click',fun1);
		this.menu.done.addEventListener('click',fun1);
	}
	makeMenuItem(name,icon,link) {
		this.menu.makeItem(name,icon,link);
	}
	makeContent() {
		// create div for main content
		this.view = this.append_element('div');
		this.view.classList.add("w3-main");
		this.view.style = "margin-left:300px;margin-top:43px;";
	}
	wipeContent() {
		while (this.view.firstChild) {
			this.view.removeChild(this.view.lastChild);
		}
	}
	makeContent_header(name,icon=null) {
		// create main content header
		let cont = this.create_element('header',this.view);
		cont.classList.add("w3-container");
		cont.style = "padding-top:22px";
		let head = this.create_element('h5',cont);
		if (icon!==null) {
			let show = this.create_element('i',head);
			show.classList.add("fa-solid");
			show.classList.add(icon);
			show.style = "margin-right:10px;";
		}
		let text = this.create_element('b',head);
		text.innerHTML = name;
	}
	makeContent_footer() {
		// create main content footer
		let cont = this.create_element('footer',this.view);
		cont.classList.add("w3-container");
		cont.classList.add("w3-padding-16");
		cont.classList.add("w3-light-grey");
		let temp = this.create_element('p',cont);
		temp.innerHTML = "Powered by ";
		let buff = this.create_element('i',temp);
		let link = this.create_element('a',buff);
		link.href = "https://www.w3schools.com/w3css/";
		link.target = "_blank";
		link.innerHTML = "w3.css";
		temp.innerHTML += " and using icons from ";
		buff = this.create_element('i',temp);
		link = this.create_element('a',buff);
		link.href = "https://fontawesome.com/";
		link.target = "_blank";
		link.innerHTML = "Font Awesome";
	}
	doMenu(show) {
		if (show===undefined||show===true) {
			if (this.menu.side.style.display === 'block') {
				this.menu.side.style.display = "none";
				this.menu.olay.style.display = "none";
			} else {
				this.menu.side.style.display = "block";
				this.menu.olay.style.display = "block";
			}
		} else if (show===false) {
			this.menu.side.style.display = "none";
			this.menu.olay.style.display = "none";
		}
	}
	onLogin() {
		this.doAlert("Welcome back, "+this.user.nick+"!","yay");
	}
	onLogout() {
		this.doAlert("Logged out","yay");
	}
	updateUser(uuid=null,name=null,nick=null) {
		let event1 = false, event0 = false;
		if (uuid==null||name==null||uuid==0) {
			if (this.user.uuid)
				event0 = true;
			this.user.reset();
		} else {
			if (!this.user.uuid&&uuid>0)
				event1 = true;
			this.user.uuid = uuid;
			this.user.user = name;
			if (nick===null) nick = name;
			this.user.nick = nick;
		}
		this.menu.update();
		if (event1) this.onLogin();
		else if (event0) this.onLogout();
	}
	doAlert(mesg,type=null) {
		let show = this.insert_element('div');
		show.classList.add("w3-panel");
		show.classList.add("w3-top");
		show.classList.add("w3-middle");
		show.classList.add("w3-small");
		let temp = "z-index:5;width:50%;left:25%;"
		temp += "padding:0;padding-left:5px;padding-right:5px;margin:0"
		show.style = temp;
		if (typeof(type)!=='string') type = "";
		switch (type) {
			case "yay": case "success": type = "w3-green"; break;
			case "err": case "error": type = "w3-red"; break;
			case "inf": case "info": type = "w3-blue"; break;
			default: type = "w3-yellow"; break;
		}
		show.classList.add(type);
		show.classList.add("w3-display-container");
		let done = this.create_element('span',show);
		done.classList.add("w3-right");
		done.setAttribute('style',"cursor:pointer;");
		done.setAttribute('onclick',"this.parentNode.remove();");
		done.innerHTML = "&times;";
		show.innerHTML += "<p>"+mesg+"</p>";
		let that = this;
		setTimeout(that.doneAlert,this.alertTime,show);
	}
	doneAlert(what) {
		what.remove();
	}
}
//------------------------------------------------------------------------------
class MY1UserBase extends MY1Base {
	constructor(desk) {
		super();
		this.desk = desk;
		this.reset();
	}
	reset() {
		this.nick = "Guest"; // nick name
		this.user = "guest"; // login name
		this.uuid = 0; // userid?
	}
}
//------------------------------------------------------------------------------
class MY1UserData extends MY1UserBase {
	constructor(desk,iurl="/api/pages/login",ourl="/api/pages/logout") {
		super(desk);
		this.desk = desk;
		this.iurl = iurl;
		this.ourl = ourl;
	}
	result_login(data) {
		if (!('flag' in data && ('data' in data || 'emsg' in data))) {
			this.show_debug("** Data:"+JSON.stringify(data));
		} else {
			if ('emsg' in data||!data.flag) {
				this.show_debug("** API Error:"+data.emsg);
			} else {
				data = data.data;
				if ('uuid' in data && 'user' in data && 'nick' in data) {
					this.show_debug("## Data:"+JSON.stringify(data));
					this.desk.updateUser(data.uuid,data.user,data.nick);
				} else {
					this.show_debug("** Invalid login! "+JSON.stringify(data));
					this.doAlert("Invalid login!","err");
				}
			}
		}
	}
	result_logout(data) {
		if (!('flag' in data && ('data' in data || 'emsg' in data))) {
			this.show_debug("** Data:"+JSON.stringify(data));
		} else {
			if ('emsg' in data||!data.flag) {
				this.show_debug("** API Error:"+data.emsg);
			} else {
				data = data.data;
				if ('stat' in data) {
					this.show_debug("## Data:"+JSON.stringify(data));
				} else {
					this.show_debug("** Invalid login! "+JSON.stringify(data));
					this.desk.doAlert("Logout issue!","err");
				}
			}
		}
		// whatever it is, we reset
		this.desk.updateUser();
	}
	doLogin(data) {
		this.readAPI = this.result_login;
		this.callAPI(this.iurl,"POST",data);
	}
	doLogout() {
		this.readAPI = this.result_logout;
		this.callAPI(this.ourl);
	}
}
//------------------------------------------------------------------------------
class MY1DeskHead extends MY1ViewBase {
	constructor(desk,title) {
		super();
		this.desk = desk;
		this.title = title;
		this.build(); // simply build!
	}
	build() {
		this.that = this.append_element('div');
		this.that.classList.add("w3-bar");
		this.that.classList.add("w3-top");
		this.that.classList.add("w3-theme-d2");
		this.that.classList.add("w3-large");
		this.that.style = "z-index:4";
		this.pick = this.desk.create_element('button',this.that,false,"site_menu");
		this.pick.classList.add("w3-bar-item");
		this.pick.classList.add("w3-button");
		this.pick.classList.add("w3-hide-large");
		this.pick.classList.add("w3-hover-none");
		this.pick.classList.add("w3-hover-text-light-grey");
		let bars = this.desk.create_element('i',this.pick);
		bars.classList.add("fa-solid");
		bars.classList.add("fa-bars");
		this.name = this.desk.create_element('span',this.that);
		this.name.classList.add("w3-bar-item");
		this.name.classList.add("w3-right");
		this.name.classList.add("w3-middle");
		this.name.innerHTML = this.title;
	}
	title(title) {
		this.title = title;
		this.name.innerHTML = title;
	}
}
//------------------------------------------------------------------------------
class MY1MenuOver extends MY1ViewBase {
	constructor(desk) {
		super();
		this.desk = desk;
		this.build();
	}
	build() {
		this.side = this.append_element('nav',"side_main");
		this.side.classList.add("w3-sidebar");
		this.side.classList.add("w3-collapse");
		this.side.classList.add("w3-white");
		this.side.classList.add("w3-animate-left");
		this.side.style = "z-index:3;width:300px;";
		// user info & menu
		this.info = this.desk.create_element('div',this.side);
		this.info.classList.add("w3-container");
		this.info.classList.add("w3-row");
		//this.desk.create_element('br',this.info);
		let temp = this.desk.create_element('div',this.info);
		temp.classList.add("w3-col");
		temp.classList.add("s4");
		this.upic = this.desk.create_element('img',temp);
		this.upic.src = "images/avatar2.png";
		this.upic.classList.add("w3-circle");
		this.upic.classList.add("w3-margin-right");
		this.upic.style = "width:46px";
		temp = this.desk.create_element('div',this.info,false,"side_user");
		temp.classList.add("w3-col");
		temp.classList.add("s8");
		temp.classList.add("w3-bar");
		this.user = this.desk.create_element('span',temp);
		this.user.classList.add("w3-bar-item");
		this.make_user_icon(temp,"fa-right-from-bracket",
			"javascript:"+this.desk.strLogout);
		//this.make_user_icon(temp,"fa-envelope");
		//this.make_user_icon(temp,"fa-user");
		// create login form
		this.form = this.desk.create_element('form',this.side,
			false,"form_login");
		this.form.action = "pages/login";
		this.form.method = "post";
		this.form.addEventListener("submit", submit_login);
		let cont = this.desk.create_element('div',this.form);
		cont.classList.add("container");
		temp = this.desk.create_element('label',cont);
		temp.innerHTML = "<b>UserID</b>";
		temp = this.desk.create_element('input',cont);
		temp.setAttribute("type","text");
		temp.setAttribute("name","username");
		temp.setAttribute("placeholder","UserID");
		temp.setAttribute("required","");
		temp = this.desk.create_element('label',cont);
		temp.innerHTML = "<b>Password</b>";
		temp = this.desk.create_element('input',cont);
		temp.setAttribute("type","password");
		temp.setAttribute("name","userpass");
		temp.setAttribute("placeholder","Password");
		temp.setAttribute("required","");
		temp = this.desk.create_element('button',cont);
		temp.setAttribute("type","Submit");
		temp.innerHTML = "Login";
		// container for side menu!
		this.cont = this.desk.create_element('div',this.side);
		this.cont.classList.add("w3-bar-block");
		// option to close menu (for small screens)
		this.done = this.desk.create_element('a',this.cont,false,"side_done");
		this.done.classList.add("w3-bar-item");
		this.done.classList.add("w3-button");
		this.done.classList.add("w3-padding-16");
		this.done.classList.add("w3-hide-large");
		this.done.classList.add("w3-dark-grey");
		this.done.classList.add("w3-hover-black");
		this.done.title = "Close Menu";
		this.done.innerHTML = "  Close Menu";
		let item = this.desk.create_element('i',this.done);
		item.classList.add("fa");
		item.classList.add("fa-remove");
		item.classList.add("fa-fw");
		// create overlay as well
		this.olay = this.append_element('div',"side_olay");
		this.olay.classList.add("w3-overlay");
		this.olay.classList.add("w3-hide-large");
		this.olay.classList.add("w3-animate-opacity");
		this.olay.style = "cursor:pointer";
		this.olay.title = "Close Side Menu";
	}
	make_user_icon(cont,icon,href=null) {
		let buff = this.desk.create_element('a',cont);
		if (href!==null) buff.href = href;
		buff.classList.add("w3-bar-item");
		buff.classList.add("w3-button");
		let item = this.desk.create_element('i',buff);
		item.classList.add("fa-solid");
		item.classList.add(icon);
	}
	makeItem(name,icon,link) {
		// create user menu
		let buff = this.desk.create_element('a',this.cont);
		if (link!=undefined&&link!=null) buff.href = link;
		buff.classList.add("w3-bar-item");
		buff.classList.add("w3-button");
		buff.classList.add("w3-padding");
		let item = this.desk.create_element('i',buff);
		item.classList.add("fa-solid");
		item.classList.add(icon);
		item.classList.add("fa-fw");
		buff.innerHTML += "  "+name;
	}
	update() {
		if (this.desk.user.uuid>0) {
			this.form.classList.add("w3-hide");
			this.info.classList.remove("w3-hide");
			this.cont.classList.remove("w3-hide");
		} else {
			this.form.classList.remove("w3-hide");
			this.info.classList.add("w3-hide");
			this.cont.classList.add("w3-hide");
		}
		// update user greeting
		this.user.innerHTML = "Welcome, <strong>"+
			this.desk.user.nick+"</strong>!";
	}
}
//------------------------------------------------------------------------------
class MY1ViewItem extends MY1ViewBase {
	constructor(elem) {
		super(elem);
		this.uurl = null;
		this.apid = null; // instanceof MY1FetchAPI/MY1GetAPI
	}
	is_visible() {
		return this.elem.classList.contains("w3-hide")?false:true;
	}
	show() {
		if (this.elem instanceof Element) {
			this.elem.classList.remove("w3-hide");
		}
	}
	hide() {
		if (this.elem instanceof Element) {
			this.elem.classList.add("w3-hide");
		}
	}
}
//------------------------------------------------------------------------------
class MY1ViewItemBlock extends MY1ViewItem {
	// create block view for my1desktop
	constructor(desk) {
		let elem = desk.create_element('div',desk.view);
		super(elem);
		this.desk = desk;
		this.elem.classList.add("w3-row-padding");
		this.elem.classList.add("w3-margin-bottom");
		this.list = [];
	}
	makeItem(bgnd,icon,data,name) {
		// create block view item
		if (this.list.length<4) {
			let quat = this.desk.create_element('div',this.elem);
			quat.classList.add("w3-quarter");
			let temp = this.desk.create_element('div',quat);
			temp.classList.add("w3-container");
			temp.classList.add(bgnd); // bg color for block
			temp.classList.add("w3-text-white");
			temp.classList.add("w3-padding-16");
			let buff = this.desk.create_element('div',temp);
			buff.classList.add("w3-left");
			let isub = this.desk.create_element('i',buff);
			isub.classList.add("fa-solid");
			isub.classList.add(icon); // icon for block
			isub.classList.add("w3-xxxlarge");
			buff = this.desk.create_element('div',temp);
			buff.classList.add("w3-right");
			let ichk = this.desk.create_element('h3',buff);
			ichk.innerHTML = data; // data for block
			buff = this.desk.create_element('div',temp);
			buff.classList.add("w3-clear");
			buff = this.desk.create_element('h4',temp);
			buff.innerHTML = name; // label for block
			let item = new MY1ViewItem(ichk);
			item.icon = isub;
			this.list.push(item);
			return item;
		} else return null;
	}
	fit() {
		let pick, loop;
		let list = this.elem.childNodes;
		for (loop=0;loop<list.length;loop++) {
			list[loop].classList.remove("w3-quarter");
			list[loop].classList.remove("w3-third");
			list[loop].classList.remove("w3-half");
		}
		switch (list.length) {
			case 2: pick = "w3-half"; break;
			case 3: pick = "w3-third"; break;
			case 4: pick = "w3-quarter"; break;
			default: pick = null;
		}
		if (pick!==null) {
			for (loop=0;loop<list.length;loop++)
				list[loop].classList.add(pick);
		}
	}
}
//------------------------------------------------------------------------------
class MY1ViewItemPanel extends MY1ViewItem {
	constructor(desk,half,view_image,view_label,list_label) {
		let elem = desk.create_element('div',desk.view);
		super(elem);
		this.desk = desk;
		this.elem.classList.add("w3-panel");
		let pads = desk.create_element('div',this.elem);
		pads.classList.add("w3-row-padding");
		pads.style = "margin:0 -16px";
		let sec1 = "w3-third";
		let sec2 = "w3-twothird";
		if (half===true) {
			sec1 = "w3-half";
			sec2 = "w3-half";
		}
		// panel to show image
		let buff = desk.create_element('div',pads);
		buff.classList.add(sec1);
		let temp = desk.create_element('h5',buff);
		temp.innerHTML = view_label;
		temp = desk.create_element('img',buff);
		temp.src = view_image;
		temp.style = "width:100%";
		// panel to show data list
		buff = desk.create_element('div',pads);
		buff.classList.add(sec2);
		temp = desk.create_element('h5',buff);
		temp.innerHTML = list_label;
		this.list = desk.create_element('table',buff);
		this.list.classList.add("w3-table");
		this.list.classList.add("w3-striped");
		this.list.classList.add("w3-white");
	}
	makeItem(icon,color,info,data) {
		// create panel list item
		let trow = this.desk.create_element('tr',this.list);
		let tcol = this.desk.create_element('td',trow);
		let temp = this.desk.create_element('i',tcol);
		temp.classList.add("fa-solid");
		temp.classList.add(icon); // icon for this item
		temp.classList.add(color); // icon color
		temp.classList.add("w3-large");
		tcol = this.desk.create_element('td',trow);
		tcol.innerHTML = info;
		tcol = this.desk.create_element('td',trow);
		temp = this.desk.create_element('i',tcol);
		temp.innerHTML = data;
	}
}
//------------------------------------------------------------------------------
class MY1ViewItemChart extends MY1ViewItem {
	// create bar chart content for my1desktop ---> WORK IN PROGRESS!!!?
	constructor(desk,name) {
		let elem = desk.create_element('div',desk.view);
		super(elem);
		this.desk = desk;
		this.elem.classList.add("w3-container");
		let temp = desk.create_element('h5',this.elem);
		temp.innerHTML = name;
	}
	makeItem(name,fgcol,bgcol,data) {
		// create bar chart item (% statistics)
		let item = this.desk.create_element('p',this.elem);
		item.innerHTML = name;
		let temp = this.desk.create_element('div',this.elem);
		temp.classList.add(bgcol); // bar chart bg color
		item = this.desk.create_element('div',temp);
		item.classList.add("w3-container");
		item.classList.add("w3-center");
		item.classList.add("w3-padding");
		item.classList.add("w3-green");
		data = parseInt(data,10);
		if (data>100) data = 100;
		else if (data<0) data = 0;
		item.style = "width:"+data+"%";
		item.innerHTML = "+"+data+"%";
	}
}
//------------------------------------------------------------------------------
class MY1ViewItemTable extends MY1ViewItem {
	// create table content for my1desktop
	constructor(desk,name) {
		let elem = desk.create_element('div',desk.view);
		super(elem);
		this.desk = desk;
		this.elem.classList.add("w3-container");
		let temp = desk.create_element('h5',this.elem);
		temp.innerHTML = name;
		this.tabs = desk.create_element('table',this.elem);
		this.tabs.classList.add("w3-table");
		this.tabs.classList.add("w3-striped");
		this.tabs.classList.add("w3-bordered");
		this.tabs.classList.add("w3-border");
		this.tabs.classList.add("w3-hoverable");
		this.tabs.classList.add("w3-white");
	}
	makeItem(name,data) {
		// content table - create table content item
		let trow = this.desk.create_element('tr',this.tabs);
		let tcol = this.desk.create_element('td',trow);
		tcol.innerHTML = name;
		tcol = this.desk.create_element('td',trow);
		tcol.innerHTML = data;
		return tcol;
	}
	moreButton(name,color) {
		// content table - add more
		this.desk.create_element('br',this.elem);
		var more = this.desk.create_element('button',this.elem);
		more.classList.add("w3-button");
		more.classList.add(color);
		more.innerHTML = name;
		var temp = this.desk.create_element('i',more);
		temp.classList.add("fa-solid");
		temp.classList.add("fa-arrow-right");
	}
}
//------------------------------------------------------------------------------
class MY1ViewItemLinks extends MY1ViewItem {
	// create links view for my1desktop
	constructor(desk) {
		let elem = desk.create_element('div',desk.view);
		super(elem);
		this.desk = desk;
		this.elem.classList.add("w3-container");
		this.elem.classList.add("w3-dark-grey");
		this.elem.classList.add("w3-padding-32");
		this.list = desk.create_element('div',this.elem);
		this.list.classList.add("w3-row");
		this.size = 0;
	}
	makeLink(label,color) {
		if (this.size<3) {
			let item = this.desk.create_element('div',this.list);
			item.classList.add("w3-container");
			item.classList.add("w3-third");
			let temp = this.desk.create_element('h5',item);
			temp.classList.add("w3-bottombar");
			temp.classList.add(color); // bar color e.g. w3-border-green
			temp.innerHTML = label;
			this.size++;
		}
	}
	getLink(pick) {
		if (pick>=0&&pick<this.size) {
			return this.list.childNodes[pick];
		} else return null;
	}
	makeLinkItem(pick,name,link) {
		if (pick>=0&&pick<this.size) {
			let that = this.list.childNodes[pick];
			let item = this.desk.create_element('p',that);
			item.innerHTML = name;
			// link to be added later
		}
	}
}
//------------------------------------------------------------------------------
