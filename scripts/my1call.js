//------------------------------------------------------------------------------
// requires: my1base.js
//------------------------------------------------------------------------------
class MY1Call extends MY1Logger {
	constructor(read=null,tdel=null) {
		super();
		this.chkwait = null; // timeout object
		this.waitAPI = null; // date object to indicate waiting for response
		this.readAPI = null; // pointer to function that reads API data!
		this.moreAPI = null; // delay for next API call - null means one-shot
		if (read!==null && typeof(read)==='function')
			this.readAPI = read;
		if (tdel!==null) this.moreAPI = parseInt(tdel);
	}
}
//------------------------------------------------------------------------------
class MY1GetAPI extends MY1Call {
	constructor(read=null,tdel=null) {
		super(read,tdel);
		this.target_ = null; // current url
		this.method_ = null; // current method
		this.chkbody = null; // current content
		/* using php script to allow access to other domain */
		this.request = "get.php"; // SHOULD NOT change this!
		this.reqxtra = "";
	}
	process(error,jdata) {
		if (error===false) this.log_error("Server Error Response: "+jdata);
		else if (parseInt(error)>0)
			this.log_error('HTTP error! => ('+error+')');
		else {
			if (this.readAPI==null)
				this.show_info("readAPI: "+JSON.stringify(jdata));
			else this.readAPI(jdata);
		}
		this.waitAPI = null;
		if (this.moreAPI!==null) {
			this.chkwait = setTimeout(()=>function(v){v.execute();},
				this.moreAPI,this);
		}
	}
	execute() {
		if (this.waitAPI!==null) return;
		if (this.chkwait!==null) {
			clearTimeout(this.chkwait);
			this.chkwait = null;
		}
		let that = this;
		let ureq = that.request+"?target="+this.target_+that.reqxtra;
		let body = this.chkbody;
		this.show_debug("CallAPIexec: "+ureq);
		MY1GetAPI.CallAPI("GET",ureq,
			()=>function(e,j){that.process(e,j);},body);
		this.waitAPI = new Date();
	}
	callAPI(target,method="GET",dobody=null) {
		if (method!=="GET"&&method!=="POST")
			method = "GET"; // default method
		this.show_debug("CallAPI:{"+method+":"+target+"}");
		this.target_ = target;
		this.method_ = method;
		this.chkbody = dobody;
		this.execute();
	}
	static CallAPI(method,target,callback,params=null) {
		// static function for api server communications
		let request = new XMLHttpRequest();
		request.open(method,target,true);
		request.setRequestHeader('Content-Type','application/json');
		request.onreadystatechange = function() {
			// Note20211101: why do i get 'undefined' in console log on 1st run?
			if (request.readyState == 4) {
				var error = 0, jdata = null;
				if (request.status == 200) {
					var responseJSON = JSON.parse(request.responseText);
					if (responseJSON!==null&&responseJSON.flag!==undefined&&
							responseJSON.flag===true) {
						jdata = responseJSON.data;
					}
					else {
						jdata = request.responseText;
						error = false;
					}
				}
				else error = request.status;
				// callback function
				if (typeof(callback)==='function')
					callback(error,jdata);
			}
		}
		request.send(params);
	}
}
//------------------------------------------------------------------------------
class MY1FetchAPI extends MY1Call {
	constructor(read=null,tdel=null) {
		super(read,null);
		this.target_ = null; // current url
		this.method_ = null; // current method
		this.chkbody = null; // current content
		this.readRAW = null; // reads raw API data (overrides readAPI)
	}
	process(resp,data) {
		this.show_debug("CallAPIproc0:{"+resp.status+"}");
		if (data===null) {
			this.show_debug("Fetch:{"+this.target_+"} => Status:"+
				resp.status+" ("+resp.statusText+")");
		}
		else if (typeof(this.readRAW)==='function') this.readRAW(data);
		else {
			let type = resp.headers.get("content-type");
			if (type.indexOf("application/json")<0) {
				this.show_debug("Fetch:{"+this.target_+"} => Status:"+
					resp.status+" ("+resp.statusText+") {Type:"+
					type+"} Raw:"+data);
			}
			else {
				let json = JSON.parse(data);
				if (!('flag' in json && ('data' in json || 'emsg' in json))) {
					// not MY api server... debug?
					this.show_debug("Fetch:{"+this.target+"} => Status:"+
						resp.status+" ("+resp.statusText+") RawJSON:"+data);
				}
				else if (typeof(this.readAPI)==='function') this.readAPI(json);
				else this.show_info("readAPI:"+JSON.stringify(json));
			}
		}
		this.waitAPI = null;
		if (this.moreAPI!==null) {
			let func = function(v) { v.execute(); }
			this.chkwait = setTimeout(func,this.moreAPI,this);
		}
	}
	execute() {
		this.show_debug("CallAPIexec0:{"+this.waitAPI+"|"+this.chkwait+"}");
		if (this.waitAPI!==null) return; // skip if previous not done
		if (this.chkwait!==null) { // if any on wait, cancel that
			clearTimeout(this.chkwait);
			this.chkwait = null;
		}
		let uurl = this.target_;
		let ureq = {
			method: this.method_,
			headers: { // always work with json format
				'Accept':'application/json',
				'Content-Type':'application/json'
			}
		};
		if (this.chkbody!==null) {
			ureq.body = this.chkbody;
		}
		this.show_debug("CallAPIexec1:{"+uurl+"|"+ureq.method+"}");
		this.fetch_server(uurl,ureq);
		this.waitAPI = new Date();
	}
	// maintain this name to make MY1FetchAPI drop in replacement for MY1GetAPI
	callAPI(target,method="GET",dobody=null) {
		if (method!=="GET"&&method!=="POST")
			method = "GET"; // default method
		this.show_debug("CallAPI:{"+method+":"+target+"}");
		this.target_ = target;
		this.method_ = method;
		this.chkbody = dobody;
		this.execute();
	}
	async fetch_server(url,req) {
		try {
			let data = null;
			let resp = await fetch(url,req);
			if (resp.status === 200) // must get OK
				data = await resp.text();
			this.process(resp,data);
		}
		catch (err) {
			this.show_debug("CallAPIfsrv:error{"+err+"}");
		}
	}
}
//------------------------------------------------------------------------------
class MY1DataAPI extends MY1FetchAPI {
	constructor(target,guiobj=null) {
		super();
		this.uurl = target;
		this.ogui = guiobj;
		this.tcnt = 0;
		this.list = [];
		this.tcnt_name = 'size';
		this.list_name = 'list';
		this.show_pick = this.tcnt_name;
	}
	read_data(data) {
		// override this  to customize
		if (data.flag) {
			data = data.data;
			let strS = this.tcnt_name;
			let strL = this.list_name;
			let strP = this.show_pick;
			if (typeof(data[strS])==='number') this.tcnt = data[strS];
			if (typeof(data[strL])==='object'&&Array.isArray(data[strL]))
				this.list = data[strL];
			if (this.ogui!==null&&typeof(data[strP])!=='undefined')
				this.ogui.innerHTML = ""+data[strP];
		}
		else this.show_debug("** Failed request!");
		this.show_debug("@@ [MY1DataAPI] Data:"+JSON.stringify(data));
	}
	get_data() {
		// always a get!
		this.readAPI = this.read_data;
		this.callAPI(this.uurl);
	}
}
//------------------------------------------------------------------------------
// sample test functions
//------------------------------------------------------------------------------
function test_api_more() {
	console.log("@@ test_api_more");
	let that = new MY1FetchAPI(null);
	that.dbug = true;
	that.moreAPI = 5000;
	that.callAPI("/api/things/random");
	setTimeout(function(v){v.moreAPI=null;},10000,that);
}
function test_api_post() {
	console.log("@@ test_api_post");
	let that = new MY1FetchAPI(null);
	that.dbug = true;
	let data = JSON.stringify({temp:24.9,tag:23});
	that.callAPI("/api/things/testtemp","POST",data);
}
//------------------------------------------------------------------------------
